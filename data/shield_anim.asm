RAnimData
	; metadata
	.byte	#%11110000			; Delta for T = 4 (4 frames per anim state)
	.byte   #%01111111			; XX11 1111 for size == 2 : XX = size - 1
	; main gfx data (address LSB)
	.byte	#ShieldSide_1		; frame 0 
	.byte	#ShieldSide_0		; frame 1
	.byte	0					; padding
	.byte	0					; padding
	; ext gfx data (address LSB)
	.byte	#ShieldSwordRight	; frame 0
	.byte	#ShieldSwordRight	; frame 1
	.byte	0					; padding
	.byte	0					; padding
	; ext gfx relative position
	.byte	#8					; frame 0
	.byte	#9					; frame 1
								; no padding needed as we will not read beyond here

LAnimData
	; metadata
	.byte	#%11110000			; Delta for T = 4 (4 frames per anim state)
	.byte   #%01111111			; XX11 1111 for size == 2 : XX = size - 1
	; main gfx data (address LSB)
	.byte	#ShieldSide_1		; frame 0 
	.byte	#ShieldSide_0		; frame 1
	.byte	0					; padding
	.byte	0					; padding
	; ext gfx data (address LSB)
	.byte	#ShieldSwordLeft	; frame 0
	.byte	#ShieldSwordLeft	; frame 1
	.byte	0					; padding
	.byte	0					; padding
	; ext gfx relative position
	.byte	#-6					; frame 0
	.byte	#-7					; frame 1
								; no padding needed as we will not read beyond here

	
	
LeftRightAnim
		.byte	#ShieldSide_1
		.byte	#ShieldSide_1
		.byte	#ShieldSide_1
		.byte	#ShieldSide_1
		.byte	#ShieldSide_0
		.byte	#ShieldSide_0
		.byte	#ShieldSide_0
		.byte	#ShieldSide_0

SwordLeftAnim
		.byte	#-6
		.byte	#-6
		.byte	#-6
		.byte	#-6
		.byte	#-7
		.byte	#-7
		.byte	#-7
		.byte	#-7

SwordRightAnim
		.byte	#8
		.byte	#8
		.byte	#8
		.byte	#8
		.byte	#9
		.byte	#9
		.byte	#9
		.byte	#9

DownAnim
		.byte	#ShieldFace_2
		.byte	#ShieldFace_2
		.byte	#ShieldFace_2
		.byte	#ShieldFace_2
		.byte	#ShieldFace_1
		.byte	#ShieldFace_1
		.byte	#ShieldFace_1
		.byte	#ShieldFace_1		

UpAnim
		.byte	#ShieldBack_2
		.byte	#ShieldBack_2
		.byte	#ShieldBack_2
		.byte	#ShieldBack_2
		.byte	#ShieldBack_1
		.byte	#ShieldBack_1
		.byte	#ShieldBack_1
		.byte	#ShieldBack_1		