ShieldSide_0
        .byte #%00000000
        .byte #%01110000
        .byte #%00000110
        .byte #%01111000
        .byte #%10000101
        .byte #%10000100
        .byte #%10000101
        .byte #%11111101
        .byte #%00000010
        .byte #%01111111
        .byte #%00011010
        .byte #%00001010
        .byte #%00001111
        .byte #%11000000
        .byte #%01111111
        .byte #%11111100


ShieldSide_1
        .byte #%00000000
        .byte #%01100000
        .byte #%10000011
        .byte #%01111000
        .byte #%10000101
        .byte #%10000101
        .byte #%10000100
        .byte #%11111101
        .byte #%00000010
        .byte #%01111111
        .byte #%00011010
        .byte #%00001010
        .byte #%00001111
        .byte #%11000000
        .byte #%11111111
        .byte #%01111100

ShieldFace_0
        .byte #%00000000
        .byte #%11100111
        .byte #%00000000
        .byte #%01110110
        .byte #%10001000
        .byte #%10001011
        .byte #%10010111
        .byte #%01100010
        .byte #%00001100
        .byte #%11111111
        .byte #%11011011
        .byte #%01011011
        .byte #%00110000
        .byte #%11000011
        .byte #%01111110
        .byte #%11111000

ShieldFace_1
        .byte #%01100000
        .byte #%11000110
        .byte #%00000000
        .byte #%01110110
        .byte #%10001000
        .byte #%10001010
        .byte #%10010111
        .byte #%01100011
        .byte #%00001100
        .byte #%11111111
        .byte #%11011011
        .byte #%01011011
        .byte #%00110000
        .byte #%11000011
        .byte #%01111110
        .byte #%11111000

ShieldFace_2
        .byte #%00000110
        .byte #%01100011
        .byte #%00000000
        .byte #%01110110
        .byte #%10001000
        .byte #%10001011
        .byte #%10010111
        .byte #%01100010
        .byte #%00001100
        .byte #%11111111
        .byte #%11011011
        .byte #%01011011
        .byte #%00110000
        .byte #%11000011
        .byte #%11111110
        .byte #%01111000

ShieldBack_0
        .byte #%00000000
        .byte #%11100111
        .byte #%00000000
        .byte #%01101110
        .byte #%00010101
        .byte #%11010101
        .byte #%11101101
        .byte #%01000110
        .byte #%00110000
        .byte #%01000010
        .byte #%10000001
        .byte #%00111100
        .byte #%01111111
        .byte #%11111000
        .byte #%01111110
        .byte #%00011100

ShieldBack_1
        .byte #%11000000
        .byte #%01100110
        .byte #%00000001
        .byte #%01101110
        .byte #%00010101
        .byte #%11010101
        .byte #%11101101
        .byte #%01000110
        .byte #%00110000
        .byte #%01000010
        .byte #%10000001
        .byte #%00111100
        .byte #%01111111
        .byte #%11111111
        .byte #%01111100
        .byte #%00011110

ShieldBack_2
        .byte #%00000011
        .byte #%01100110
        .byte #%10000000
        .byte #%01101110
        .byte #%00010101
        .byte #%01010101
        .byte #%11101101
        .byte #%01000110
        .byte #%00110000
        .byte #%01000010
        .byte #%10000001
        .byte #%00111100
        .byte #%01111111
        .byte #%11111000
        .byte #%01111110
        .byte #%00011100
		
ShieldSideFaceColor
		.byte	#$16		
		.byte	#$16		
		.byte	#$16		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$4C		
		.byte	#$4C		
		.byte	#$0E		
		.byte	#$0E		
		.byte	#$4C		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		

ShieldBackColor
		.byte	#$16		
		.byte	#$16		
		.byte	#$16		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$4C		
		.byte	#$4C		
		.byte	#$4C		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8		
		.byte	#$C8

