	
ShieldSwordLeft
	.byte	#-4								;Y position		
	.byte	#0
	.byte	#0		
	.byte	#BALL_ENABLE | HMOVE_RIGHT_1	
	.byte	#BALL_ENABLE		
	.byte	#BALL_ENABLE | HMOVE_RIGHT_1		
	.byte	#BALL_ENABLE		
	.byte	#BALL_ENABLE | HMOVE_RIGHT_1
	.byte	#BALL_ENABLE | HMOVE_RIGHT_3		

ShieldSwordLeftWidth
	.byte	#PF_MIRROR 
	.byte	#PF_MIRROR
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_2
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_2
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_1

ShieldSwordRight
	.byte	#-4		
	.byte	#0
	.byte	#0
	.byte	#BALL_ENABLE
	.byte	#BALL_ENABLE | HMOVE_LEFT_1			
	.byte	#BALL_ENABLE
	.byte	#BALL_ENABLE | HMOVE_LEFT_1
	.byte	#BALL_ENABLE | HMOVE_LEFT_1 
	.byte	#BALL_ENABLE | HMOVE_RIGHT_4		

ShieldSwordRightWidth
	.byte	#PF_MIRROR 
	.byte	#PF_MIRROR
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_2
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_2
	.byte	#PF_MIRROR | BALL_SIZE_1
	.byte	#PF_MIRROR | BALL_SIZE_1
