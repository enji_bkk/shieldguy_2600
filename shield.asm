;milquetoast the ghost
;by Kirk Israel
;a cute ghost. press button to say boo!


	processor 6502
	include u:\a2600\asm\include\vcs.h
	include u:\a2600\asm\include\macro.h
	include u:\a2600\asm\include\hmove_consts.h

	include data\shield_anim_const.h

;label some variables....
	SEG.U VARS
	ORG $80

; "register" (tmp variables)
DR0	ds	1
DR1	ds	1
DR2 ds	1
DR3 ds	1
AR0	ds	2
AR1	ds	2

;the ghosts height will be in 2 bytes;
;low, high (fractional byte, than integer byte)
;drawing onscreen, we only care about the integer byte
;but we will keep track of both
P0_state 			ds 1	;b0,1 : orientation 00=N, 01=S, 10=E, 11=W
							;b2 : move
							;b3 : hit
							;b4 : strike

P0_XPos 			ds 2	;horizontal position
P0_YPosFromBot 		ds 2
P0_animFrameCounter	ds 1 	;count frames to time animations (switch between states)

P0_Y 				ds 1	;needed for skipdraw
P0_Ptr 				ds 2	;ptr to current graphic
P0_colorPtr 		ds 2 	;ptr to current color table
P0_EXT_X			ds 1	;X position for the ball to start drawing it
P0_EXT_PTR			ds 2	;Pattern to move the ball around to draw the sword							

P_X_POS_OFFSET   = P0_XPos - P0_state
P_COUNTER_OFFSET = P0_animFrameCounter - P0_state
P_GFX_PTR_OFFSET = P0_Ptr - P0_state
P_EXT_POS_OFFSET = P0_EXT_X - P0_state
P_EXT_PTR_OFFSET = P0_EXT_PTR - P0_state

PF_ID				ds 1	;the ID of currently displayed area

	SEG CODE
	org $1000

; stack pointer reset
	ldx #$FF
	txs                    ; initialize stack pointer

;a few constants...
C_GHOST_SPEED = 255		;subpixel, divide by 256 for pixel speed
C_P0_HEIGHT = 16		;height of P0 sprite
C_P0_EXT_HEIGHT = 8		;size of P0 sprite extension (ball gfx)
C_KERNAL_HEIGHT = 96	;height of kernal (full screen, 2 lines kernal)

C_S_EAST   = %10000000	; 4 directions : same as joystick state
C_S_WEST   = %01000000
C_S_SOUTH  = %00100000
C_S_NORTH  = %00010000
C_S_MOVE   = %00001000
C_S_HIT    = %00000100
C_S_STRIKE = %00000010

Start
	CLEAN_START

	;black background, white ghost...
	lda #$00
	sta COLUBK
	sta P0_animFrameCounter
	lda #$01
	sta P0_state
	lda #$0F
	sta COLUP0
	

	lda #32
	sta P0_YPosFromBot+1	;Initial Y Position in integer part of 2 byte speed

	lda #90
	sta P0_XPos

	lda #<ShieldSide_0		;low byte of ptr is normal graphic
	sta P0_Ptr				;(high byte already set)
	
	lda #>ShieldSideFaceColor ;high byte of graphic location
	sta P0_colorPtr+1		;store in high byte of graphic pointer
	
	lda #>ShieldSwordLeft
	sta P0_EXT_PTR+1
	lda #<ShieldSwordLeft	;remove when all states are handled
	sta P0_EXT_PTR

	lda #PF_MIRROR  ; Mirrored Playfield
    sta CTRLPF 

	sta VDELP0
	sta VDELBL
	
MainLoop
	VERTICAL_SYNC
	lda #43
	sta TIM64T

	;======================================================
	; Prepare PF (if screen has changed)

	; PF ready
	;======================================================

	;======================================================
	; Player data
	
	; Reset the MSB of the pointer. It will be adjusted later
	lda #>ShieldSide_0 ;high byte of graphic location
	sta P0_Ptr+1	;store in high byte of graphic pointer

	; Preset AR1 with P0 address
	lda #<P0_state
	sta AR1
	lda	#>P0_state			; today : always 0, but could be in extra RAM
	sta AR1+1

	;======================================================
	;joystick pressed left?
	lda #C_S_WEST
	bit SWCHA
	bne DoneMoveLeft

	; Can we move left ? (do it with X to preserve A)
	ldx P0_XPos
	cpx #8
	bne DoMoveLeft
	sta P0_state
	jmp DoneMoveLeft
DoMoveLeft
	; Move
	dec P0_XPos				;move player right

	lda #<LAnimData
	sta AR0
	lda #>LAnimData
	sta AR0+1
	
	; If was not moving left previously :
	; - set the animation frame to the 1st walking frame
	; - reset the frame counter
	lda P0_state
	and #C_S_MOVE|C_S_WEST
	cmp #C_S_MOVE|C_S_WEST		; set the Z flag accordingly. Will be used in SelectAnimFrame to know if reset is needed.
	jsr SelectAnimFrame

	lda #C_S_MOVE|C_S_WEST	; set state to moving left
	sta P0_state

	lda #<ShieldSideFaceColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		; (high byte already set)
	
	lda #%00001000  ;a 1 in D3 of REFP0 says make it mirror
	sta REFP0
	jmp DoneSpriteSelect
	; Done Move Left
	;======================================================

DoneMoveLeft

	;======================================================
	;joystick pressed right?
	lda #C_S_EAST
	bit SWCHA
	bne DoneMoveRight

	; Can we move right ?
	lda P0_XPos
	cmp #152
	beq DoneMoveRight

	inc P0_XPos		;move player right

	lda #<RAnimData
	sta AR0
	lda #>RAnimData
	sta AR0+1

	; If was not moving right previously :
	; - set the animation frame to the 1st walking frame
	; - reset the frame counter
	lda P0_state
	and #C_S_MOVE|C_S_EAST
	cmp #C_S_MOVE|C_S_EAST
	jsr SelectAnimFrame	
	
	lda #C_S_MOVE|C_S_EAST	; set state to moving right
	sta P0_state
	
	lda #<ShieldSideFaceColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		;(high byte already set)

	lda #%00000000
	sta REFP0    	;unmirrored P0
	jmp DoneSpriteSelect
	; Done Move Right
	;======================================================


DoneMoveRight

	;======================================================
	;joystick pressed Down?
	lda #%00100000
	bit SWCHA
	bne DoneMoveDown
	
	lda P0_YPosFromBot+1
	cmp #C_P0_HEIGHT
	beq DoneMoveDown

	; If was not moving Down previously :
	; - set the animation frame to the 1st walking frame
	; - reset the frame counter
	lda P0_state
	and #C_S_MOVE|C_S_SOUTH
	sec
	sbc #C_S_MOVE|C_S_SOUTH
	beq Down_noFrameReset	; we were moving Down, so skip
	
	lda #C_S_MOVE|C_S_SOUTH	; set state to moving Down
	sta P0_state

	lda #7					; reset to begin animation loop
	sta P0_animFrameCounter
	
	jmp Down_NotLastFrame	; animation initialized, skip the remaining
	
Down_noFrameReset

	; we were moving Down. Check frame counter to know if
	dec P0_animFrameCounter
	lda P0_animFrameCounter
	bne Down_NotLastFrame
	lda #7					; reset to begin animation loop
	sta P0_animFrameCounter

Down_NotLastFrame
	; at this stage : animation data updated, A contains new value for P0_animFrameCounter i.e.: the index
	; in the animation table. Transfer to X to read the pointer for this animation step
	tax
	lda DownAnim,X
	sta P0_Ptr
	
	lda #<ShieldSideFaceColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		;(high byte already set)

	lda #<ShieldSwordRight
	sta P0_EXT_PTR

	;16 bit math, subtract both bytes
	;of the ghost speed constant to
	;the 2 bytes of the position
	sec
	lda P0_YPosFromBot
	sbc #<C_GHOST_SPEED
	sta P0_YPosFromBot
	lda P0_YPosFromBot+1
	sbc #>C_GHOST_SPEED
	sta P0_YPosFromBot+1

	clc
	lda SwordRightAnim,X	; read top left X coordinate of the sword relative to the player
	adc P0_XPos
	sta P0_EXT_X
	
	lda #%00000000
	sta REFP0    	;unmirrored P0
	jmp DoneSpriteSelect
	; Done Move Down
	;======================================================

	
DoneMoveDown
	;======================================================
	; Move Up (north)
	lda #%00010000	;Up?
	bit SWCHA
	bne DoneMoveUp

	lda P0_YPosFromBot+1
	cmp #88
	beq DoneMoveUp
	
	; If was not moving Up previously :
	; - set the animation frame to the 1st walking frame
	; - reset the frame counter
	lda P0_state
	and #C_S_MOVE|C_S_NORTH
	sec
	sbc #C_S_MOVE|C_S_NORTH
	beq Up_noFrameReset	; we were moving Up, so skip
	
	lda #C_S_MOVE|C_S_NORTH	; set state to moving Up
	sta P0_state

	lda #7					; reset to begin animation loop
	sta P0_animFrameCounter
	
	jmp Up_NotLastFrame	; animation initialized, skip the remaining
	
Up_noFrameReset

	; we were moving Up. Check frame counter to know if
	dec P0_animFrameCounter
	lda P0_animFrameCounter
	bne Up_NotLastFrame
	lda #7					; reset to begin animation loop
	sta P0_animFrameCounter

Up_NotLastFrame
	; at this stage : animation data updated, A contains new value for P0_animFrameCounter i.e.: the index
	; in the animation table. Transfer to X to read the pointer for this animation step
	tax
	lda UpAnim,X
	sta P0_Ptr
	
	lda #<ShieldBackColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		;(high byte already set)
	
	lda #<ShieldSwordLeft
	sta P0_EXT_PTR

	;16 bit math, add both bytes
	;of the ghost speed constant to
	;the 2 bytes of the position
	clc
	lda P0_YPosFromBot
	adc #<C_GHOST_SPEED
	sta P0_YPosFromBot
	lda P0_YPosFromBot+1
	adc #>C_GHOST_SPEED
	sta P0_YPosFromBot+1
	
	clc
	lda SwordLeftAnim,X	; read top left X coordinate of the sword relative to the player
	adc P0_XPos
	sta P0_EXT_X
	jmp DoneSpriteSelect
	; Done move up
	;======================================================


DoneMoveUp

	;check firebutton
	ldx INPT4
	bmi MouthIsClosed ;(button not pressed)
MouthIsOpen
	lda #<ShieldSide_1 	;low byte of ptr is boo graphic
	;sta P0_Ptr		;(high byte already set)
	jmp DoneWithMouth
MouthIsClosed
	lda #<ShieldSide_0 	;low byte of ptr is normal graphic
	;sta P0_Ptr		;(high byte already set)
DoneWithMouth
	;both cases : pointer to the color table

	
DoneAllMove
	; If we had to move, or strike, or be hit, we did and we set P0_ptr at the time
	; If we did not move : time to select the correct static sprite, based on orientation
	; Erase the corresponding flags
	
	lda P0_state
	and #$ff ^ [ C_S_HIT | C_S_MOVE | C_S_STRIKE ]
	sta P0_state
	
	lda #0
	sta P0_animFrameCounter
	
	;====================================================
	; Stand facing east or west
	lda #C_S_WEST|C_S_EAST
	bit P0_state
	beq DoStandingNorth
	
	lda #<ShieldSide_0
	sta P0_Ptr
	
	lda #<ShieldSideFaceColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr				;(high byte already set)

	; Pattern and position to draw the sword depends on east or west
	lda #C_S_WEST
	bit P0_state
	beq DoStandingEast

	;-----------------------------------
	; Face West
	lda #<ShieldSwordLeft
	sta P0_EXT_PTR

	clc
	lda SwordLeftAnim
	adc P0_XPos
	sta P0_EXT_X

	lda #%00001000  ;a 1 in D3 of REFP0 says make it mirror
	sta REFP0

	jmp DoneSpriteSelect
	
	;-----------------------------------
	; Stand facing east
DoStandingEast
	
	lda #<ShieldSwordRight
	sta P0_EXT_PTR

	clc
	lda SwordRightAnim
	adc P0_XPos
	sta P0_EXT_X

	lda #%00000000  ;a 1 in D3 of REFP0 says make it mirror
	sta REFP0

	jmp DoneSpriteSelect
	; End face east or west
	;====================================================

	;====================================================
	; Stand facing North
DoStandingNorth
	; facing north ?
	lda #C_S_NORTH
	bit P0_state
	beq DoStandingSouth

	lda #<ShieldBack_0
	sta P0_Ptr
	
	lda #<ShieldBackColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		;(high byte already set)

	lda #<ShieldSwordLeft
	sta P0_EXT_PTR
	
	clc
	lda SwordLeftAnim
	adc P0_XPos
	sta P0_EXT_X

	lda #%00000000  ;a 1 in D3 of REFP0 says make it mirror
	sta REFP0
	
	jmp DoneSpriteSelect

	; End facinf North
	;====================================================

	;====================================================
	; Stand facing South

DoStandingSouth
	; facing south ? yes by default

	lda #<ShieldFace_0
	sta P0_Ptr
	
	lda #<ShieldSideFaceColor 	;low byte of ptr is normal graphic
	sta P0_colorPtr		;(high byte already set)

	lda #<ShieldSwordRight
	sta P0_EXT_PTR

	clc
	lda SwordRightAnim
	adc P0_XPos
	sta P0_EXT_X

	lda #%00000000  ;a 1 in D3 of REFP0 says make it mirror
	sta REFP0

	; End facing South
	;====================================================

DoneSpriteSelect

	;====================================================
	; push player data to the stack for faster retrieve
	; during scanline

	; Prepare data, pointers, ... Assume all data in same page, adjust only the LSB of the addresses
	ldy #0					; loop counter and index in tables

	lda (P0_EXT_PTR),y		; TODO: Store the 2-complement of y position : DR0 <- -Y (stored as is in gfx table) 
	sta DR0
	inc P0_EXT_PTR			; Point to the beginning of ext gfx data
	;inc P0_EXT_PTR			; increment once more else y will not have the correct value
	
	clc						; shift the pointer so that when we add y later. We have -Y so actually perform an add 
	lda P0_EXT_PTR			;
	adc DR0					;
	sta P0_EXT_PTR

	clc
	adc #8					; pointer to the ball width table : add 8 to the previous one
	sta AR0
	lda P0_EXT_PTR+1			; MSB
	sta AR0+1
	
PushLoopBegin

	lda	#C_P0_EXT_HEIGHT-1
	cmp DR0
	bcs DoPushExt
	lda #PF_MIRROR
	pha
	lda #0
	jmp DonePushExt
DoPushExt
	lda (AR0),y
	pha
	lda (P0_EXT_PTR),y
DonePushExt
	pha						; still push the last value

	lda (P0_Ptr),y			; always push main gfx
	pha

	inc	DR0
	iny
	cpy #C_P0_HEIGHT
	bne PushLoopBegin

	;====================================================
	;for Battlezone style exact horizontal repositioning
	;subroutine as rediscovered by R. Mundschau and explained by Andrew Davie,
	;set A = desired horizontal position, then X to object
	;to be positioned (0->4 = P0->BALL)
	lda P0_XPos
	ldx #0
	jsr bzoneRepos

	lda P0_EXT_X
	ldx #4
	jsr bzoneRepos

	;for skipDraw, P0_Y needs to be set (usually during VBLANK)
	;to Vertical Position (0 = top) + height of sprite - 1.
	;we're storing distance from bottom, not top, so we have
	;to start with the kernal height and YPosFromBot...
	lda #C_KERNAL_HEIGHT + #C_P0_HEIGHT - #1
	sec
	sbc P0_YPosFromBot+1 ;subtract integery byte of distance from bottom
	sta P0_Y

	; Prepare data for P0 display : data that was not pre-loaded on the stack
	; That leaves the color
	lda #C_P0_HEIGHT-#1
	sec
	sbc P0_YPosFromBot+1	; A contains number to add to P0_Ptr (Negative since P0_YPosFromBot > #C_P0_HEIGHT-#1 most of the time)

	clc
	adc P0_colorPtr
	sta P0_colorPtr


WaitForVblankEnd
	lda INTIM
	bne WaitForVblankEnd
	ldy #C_KERNAL_HEIGHT - 1; (off by one error)

	sta WSYNC
	sta HMOVE ;move objecs that were finely positioned

	sta VBLANK

; Top screen kernel : status bar
TopKernel
	
	dey
	dec P0_Y

	lda #$44
	sta COLUPF

	sty PF1
	
	cpy #C_KERNAL_HEIGHT-8


	sta WSYNC
	
	bne TopKernel

	sta HMCLR
	lda #$4
	sta COLUPF

	ldx		Room_00_PF2_left-1,y	; 4=68

	jmp MainKernelBegin
	
		org $1a00
MainKernelBegin
	sta WSYNC		
; Main kernel : displays the game area
MainKernel_line1

; playfield (PF2 already done end of last kernel)
	stx		PF2						; 3
	ldx		Room_00_PF1_left-1,y	; 4=7
	stx		PF1				; 3=10
	
; draw player sprite 0:
	lda     #C_P0_HEIGHT-1	; 2=12
	dcp     P0_Y            ; 5=17
	bcs     .doDraw0        ; 2=19		/ 3=20
	lda     #0              ; 2=21
	sta		GRP0			; 3=24
	lda		#HMOVE_NO_MOVE | BALL_DISABLE	; 2=26
	sta		HMBL			; 3=29
	sta		ENABL			; 3=32
	lda		#00000001		; 2=34
	sleep	4				; 4=38
	jmp .doDraw0Next		; 3=41
.doDraw0
	pla						;			/ 4=24
	sta     GRP0            ; 			/ 3=27
	pla						;           / 4=31
	sta		HMBL			;           / 3=34
	sta		ENABL			;           / 3=37
	pla						;			/ 4=41
.doDraw0Next

	
; playfield second half
	ldx		Room_00_PF2_right-1,y	; 4=45 MUST start at 41
	stx		PF2					; 3=48
	ldx		Room_00_PF1_right-1,y	; 4=52
	stx		PF1					; 3=55


	sleep	10					;10=65 
	
	;always update color for the moment... no biggy if we read garbage
	sta		CTRLPF				; 3=68

	lda     (P0_colorPtr),y		; 5=73
	sta     COLUP0				; 3=76
	
MainKernel_line2

	sta	HMOVE						; 3
	
	lda #0							; 2=5
	sta GRP1						; 3=8

	ldx		Room_00_PF1_left-1,y	; 4=12
	stx		PF1						; 3=15
	ldx		Room_00_PF2_left-1,y	; 4=19
	stx		PF2						; 3=22

	sleep	19						;19=41
	

	ldx		Room_00_PF2_right-1,y	; 4=45
	stx		PF2						; 3=48
	ldx		Room_00_PF1_right-1,y	; 4=52
	stx		PF1						; 3=55

	sleep	12						;12=67

	
	; some playfield data for the next line
	ldx		Room_00_PF2_left-2,y		; 4=71  remove -1, then can move dey after, and remove cpy

	dey								; 2=73
	bne MainKernel_line1			; 3=76

	
	lda #0		; clear the PF not to write into the status bar
	sta PF0
	sta PF1
	sta PF2
	
	lda #2
	sta WSYNC
	sta VBLANK
	ldx #30
OverScanWait
	sta WSYNC
	dex
	bne OverScanWait
	jmp  MainLoop

;=====================================================
;	Routine to move to next animation frame
;	input:	AR0 : address of the player data to update
;			AR1 : address of the current animation
;=====================================================
SelectAnimFrame
	beq	SAF_SkipInitCounter	; if Z flag is set : initialize the anim counter
	ldy #ANIM_SIZE			; read the counter value from gfx data
	lda	(AR0),y
	jmp SAF_InitCounterDone
SAF_SkipInitCounter
	ldy	#P_COUNTER_OFFSET	; Y is the relative pointer
	lda	(AR1),y				; Read the counter for selected player

SAF_InitCounterDone			; Either way : counter init has been done.
	ldy #ANIM_DELTA
	clc
	adc (AR0),y				; decrement counter
	bcs SAF_NoOvf			; if counter overflows : reset to anim size
	
	ldy #ANIM_SIZE			; read size                             : CC111111
	and (AR0),y				; and with counter that just overflowed : 11XXXXXX
SAF_NoOvf
	ldy #P_COUNTER_OFFSET
	sta (AR1),y				; store new value of counter			: CCXXXXXX
							; A contains the current counter value, format : (c) CCXXXX00
	rol						; (C) CXXXXXXc
	rol						; (C) XXXXXXcC
	rol						; (X) XXXXXcCC
	and	#%00000011			; (X) 000000CC => the value by which to shift AR0 to point the good frame of gfx data
	clc
	adc AR0					; update the pointer to the correct frame offset : Y can be filled with immediate values
	sta AR0					; Assume no page boundary crossed
	
	ldy #ANIM_GFX			; stay at gfx data
	lda (AR0),y
	ldy #P_GFX_PTR_OFFSET
	sta (AR1),y

	ldy #ANIM_NB_FRAMES+ANIM_GFX	; shift to ext gfx relative position
	lda (AR0),y
	ldy #P_EXT_PTR_OFFSET
	sta (AR1),y
	
	ldy #2*ANIM_NB_FRAMES+ANIM_GFX	; shift to ext gfx relative position
	lda (AR0),y
	ldy	#P_X_POS_OFFSET		; Add the current position of the player
	adc	(AR1),y				; assume no carry (should still be unset after last adc
	ldy #P_EXT_POS_OFFSET
	sta (AR1),y

	rts
	
	;Battlezone style exact horizontal repositioning
	;subroutine as rediscovered by R. Mundschau and explained by Andrew Davie,
	;set A = desired horizontal position, then X to object
	;to be positioned (0->4 = P0->BALL)
bzoneRepos
	sta WSYNC                   ; 00     Sync to start of scanline.
	sec                         ; 02     Set the carry flag so no borrow will be applied during the division.
.divideby15
	sbc #15                     ; 04     Waste the necessary amount of time dividing X-pos by 15!
	bcs .divideby15             ; 06/07  11/16/21/26/31/36/41/46/51/56/61/66

	tay
	lda fineAdjustTable,y       ; 13 -> Consume 5 cycles by guaranteeing we cross a page boundary
	sta HMP0,x

	sta RESP0,x                 ; 21/ 26/31/36/41/46/51/56/61/66/71 - Set the rough position.
	rts
;-----------------------------
; This table converts the "remainder" of the division by 15 (-1 to -15) to the correct
; fine adjustment value. This table is on a page boundary to guarantee the processor
; will cross a page boundary and waste a cycle in order to be at the precise position
; for a RESP0,x write

            ORG $1b00
fineAdjustBegin

            DC.B %01110000 ; Left 7
            DC.B %01100000 ; Left 6
            DC.B %01010000 ; Left 5
            DC.B %01000000 ; Left 4
            DC.B %00110000 ; Left 3
            DC.B %00100000 ; Left 2
            DC.B %00010000 ; Left 1
            DC.B %00000000 ; No movement.
            DC.B %11110000 ; Right 1
            DC.B %11100000 ; Right 2
            DC.B %11010000 ; Right 3
            DC.B %11000000 ; Right 4
            DC.B %10110000 ; Right 5
            DC.B %10100000 ; Right 6
            DC.B %10010000 ; Right 7

fineAdjustTable EQU fineAdjustBegin - %11110001 ; NOTE: %11110001 = -15

	; adjust table : aligns the ext graphic data in the page correctly
	include data\shield_sword.asm

	org $1c00

	include data\shield_sprite.asm

	org $1d00

	include data\shield_anim.asm

	org $1e01

	include data\shield_room00.asm
		
	org $1FFC
	.word Start
	.word Start